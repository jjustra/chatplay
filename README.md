# chatplay

Simple one-file html program to replay your chat sessions.

You can put your log :

- into textarea
- drag'n'drop it as a whole file
- include any amount of them in chatplay.logs.js file, using directoryPush(title, text) calls
- insert them directly into html as PRE tag (see: around line 50 in chatplay.html)

Options include :

- 5 degrees of replay speed
- color/monochrome theme
- play/pause at will
- background image
- drag'n'drop .txt file / bg image file
- preload any chat logs by chatplay.logs.js file

Here is online [demo](http://apps.kajutastudio.cz/chatplay.html).

Expected format :

```
Me: Hello there.
General Grievous: General Kenobi. You are the bold one.
```
